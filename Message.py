# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 17:13:13 2020

@author: Alexia Rousset
"""
#TERMINE!!!

#!/usr/bin/env python3
import json
import time
import random
from hashlib import sha256

class Message:
    #the constructor can take various parameters
    #default is parameterless, it generates a message to be said
    #if type is not specified, it is to be imported from an export string or parsed export string
    #otherwise, if date is False the time is system time
    #else it is a float, then the date should be this parameter
    
    #Definition des types de messages
    MSG_THEY=0
    MSG_IHEARD=1
    MSG_ISAID=2
    
    #Constructeur
    def __init__(self, msg="", msg_type=False, msg_date=False):
        if msg=="":#Si pas d'ID renseigne dans la declaration du message
            self.msg_date=time.time()#Date de création du message
            self.generate()#Génération d'un message avec un ID aleatoire
        else : #Si ID renseigne
            self.msg=msg #Reprise de l'ID du constructeur
            self.msg_date=time.time() #Date actuelle
            
        if msg_type==False: #Si pas de type de message spécifié
            self.msg_type=self.MSG_THEY #Message They said par défaut
        else : #Sinon
            self.msg_type=msg_type #Attribut du type spécifié dans le constructeur
            

    #a method to compute the age of a message in days or in seconds
    def age(self, days=True):
        age=time.time()-self.msg_date#Calcul de l'age en seconde
        if days:#Si on veut l'age en jours
            return(age/(3600*24)) #Conversion secondes--> jours
        else :
            return age
  
    #a class method that generates a random message
    #@classmethod
    def generate(self):
        msg=str(int(self.msg_date*100000000))#Creation d'un ID a partir de la date actuelle
        for i in range(0,4):
            msg=msg+str(random.randint(0,9))#Ajout de chiffres aleatoires
        self.msg=msg
        return msg
        
        
    
    #a method to convert the object to string data 
    def __str__(self):
        pass #Nous n'avons pas utilise cette methode, nous avons directement utilisé str(objet) 
    
    #export/import
    def m_export(self):
        #Exporte le message
        #Fait dans MessageCatalog
        
        pass
    def m_import(self, msg):
        #Importe le message
        #Fait dans MessageCatalog
        pass
    
#will only execute if this file is run
if __name__ == "__main__": #Executé que quand fichier Python éxécuté
    #test the class
    myMessage = Message() #Créartion d'un premier message
    print("Message1")
    #Affichage des informations sur le message
    print(myMessage.msg)
    print(myMessage.msg_date)
    print(myMessage.msg_type)
    print()
    time.sleep(1)#Attente 1 seconde
    mySecondMessage = Message("",myMessage.MSG_ISAID)  #Création d'un second message en spécifiant des éléments dans le constructeur
    print("Message2")
    #Affichage des infos sur le message
    print(mySecondMessage.msg)
    print(mySecondMessage.msg_date)
    print(mySecondMessage.msg_type)
    




