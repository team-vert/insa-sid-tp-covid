# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:52:08 2020

@author: Alexia Rousset
"""
#TERMINE!!!

#!/usr/bin/env python3
from Message import Message
from MessageCatalog import MessageCatalog
import requests
import json

class Client:
    #constructor, takes a message catalog
    def __init__(self, catalog, debug = False):
        self.catalog=MessageCatalog(catalog) #Creation catalogue client de messages
		
    #send an I said message to a host
    def say_something(self, host):
        myMessage=Message("",Message.MSG_ISAID)#Creation d'un message ISAID
        r=requests.post(host+"/"+myMessage.msg)#Envoi du message a l'hopital
        if r.status_code == 201: #Si la requete est reussie et qu'une ressource a ete cree en consequence
            self.catalog.add_message(myMessage)#Ajout du message au catalogue client
            #print("YES")
        else : #Affichage du statut d'erreur si la requete n'a pas fonctionne
            print(r.status_code)
        
		
    #add to catalog all the covid patient messages from host server
    def get_covid(self, host):
        r=requests.get(host+'/they-said') #Requete demandant les messages envoyes par les patients COVID a l'hopital
        if r.status_code == 200:#Si la requete a fonctionne
            self.catalog.c_import(r.json())#On importe les messages dans le catalogue client
        else: #Si la requete ne fonctionne pas
            print(r.status_code)
            
    #send to a server the list of “I said” messages
    def send_history(self, host): 
        self.catalog.purge(msg_type=Message.MSG_ISAID) #Supprime tous les messages que le client a dit trop ancien
        data =self.catalog.c_export_type(Message.MSG_ISAID) #Exporter tous les messages ISAID dans un fichier JSON
        r=requests.post(host+'/they-said',json=data) #Envoyer le fichier JSON au serveur de l'hopital


if __name__ == "__main__":
   c = Client("test4.json")
   c.say_something("http://localhost:5000")
   #print("Taille",len(c.catalog.catalogue))
   c.get_covid("http://localhost:5000")
   #print('je suis ici')
   c.send_history("http://localhost:5000")
   
