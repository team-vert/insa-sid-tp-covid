# -*- coding: utf-8 -*-
"""
Created on Sat Nov  7 18:07:22 2020

@author: Alexia Rousset
"""

#TERMINE!!!

#!/usr/bin/env python3
from Message import *
import json
import os

class MessageCatalog:
    #constructor, loads the messages (if there are any)
    def __init__(self, filepath):
        self.filepath=filepath #Chemin ou on peut trouver le fichier contenant les messages
        self.etat=(os.path.exists(self.filepath) and os.path.getsize(self.filepath)!=0) #Dis si un fichier contenant des messages existe et est non vide 
 
        self.catalogue=[]#Creation catalogue de messages vides
        if self.etat==True:#Si un fichier existe déjà on importe les messages situés dans ce fichier
            self.c_import()
       
        
    #destructor (optional, if you opened a file, close it)
    def __del__(self):
        #Non utilise
        pass
    
    #Import object content (can import batch of THEY SAID or other for testing purposes)
    def c_import(self, fichierjson=None): #Gere le stockage des messages : importe chaque message individuellement 
        exist=False#Permet de savoir si on créé un fichier input_json à fermer à la fin de la fonction
        if fichierjson == None:#Si on ne prend pas un fichier json en entrée (c'est a dire si on recupere le fichier json via son adresse)
            input_json=open(self.filepath,'r')
            fichierjson=json.load(input_json) #importer les donnees
            exist=True
        for i in fichierjson: #Parcours des donnees
            self.add_message(Message(i["msg"],i["msg_type"],i["msg_date"]))  #Ajout des messages au catalogue
        if exist==True: #Fermeture du fichier json ouvert
            input_json.close()
        
     #Import object content d'un type
    def c_import_type(self,msg_type,fichierjson=None): #Gère le stockage des messages : importe chaque message individuellement et change leur type (utilise dans la classe app)
        exist=False#Permet de savoir si on créé un fichier input_json à fermer à la fin de la fonction
        if fichierjson == None:#Si on ne prend pas un fichier json en entrée
            input_json=open(self.filepath,'r')
            exist=True
            fichierjson=json.load(input_json)
        for i in fichierjson:
            self.add_message(Message(i["msg"],msg_type,i["msg_date"]))  #Ajout des messages au catalogue en changeant leur type
        if exist==True:
            input_json.close()
            
    #export the messages of a given type from a catalog
    def c_export_type(self, msg_type): 
        fichierjson=open(self.filepath,'w')#Cree un fichier json
        donnees="["
        for i in self.catalogue :#Transfere les données sous format JSON dans la variable donnees
            if i.msg_type==msg_type: #N'exporte que les messages d'un type donne
                if donnees=="[":# Pas de virgule au debut pour le premier message pour la formalisation JSON
                    donnees=donnees +f"{{\n \"msg\":\"{i.msg}\",\n \"msg_type\":{i.msg_type},\n \"msg_date\":{i.msg_date}\n}}"
                else :
                    donnees=donnees +f",\n{{\n \"msg\":\"{i.msg}\",\n \"msg_type\":{i.msg_type},\n \"msg_date\":{i.msg_date}\n}}"
        donnees=donnees+"]"
        fichierjson.write(donnees)#Ecrit les donnees dans le fichier JSON
        fichierjson.close()
        return donnees
        
    
    #Export the whole catalog under a text format (
    #Idem que export_type sans selection des messages
    def c_export(self): #Gère le stockage des messages : exporte chaque message individuellement
        fichierjson=open(self.filepath,'w')#Cree un fichier json
        donnees="["
        for i in self.catalogue :#Transfere les données sous format JSON
            if donnees=="[":
                donnees=donnees +f"{{\n \"msg\":\"{i.msg}\",\n \"msg_type\":{i.msg_type},\n \"msg_date\":{i.msg_date}\n}}"
            else :
                donnees=donnees +f",\n{{\n \"msg\":\"{i.msg}\",\n \"msg_type\":{i.msg_type},\n \"msg_date\":{i.msg_date}\n}}"
        donnees=donnees+"]"
        fichierjson.write(donnees)#Ecrit les donnees dans le fichier JSON
        fichierjson.close()
        return donnees

    
    #add a Message object to the catalog
    def add_message(self, m, save=True):
        self.catalogue.append(m)

    
    #remove all messages of msg_type that are older than max_age days
    #msg_type is false for deleting all messages of all types older than max_age
    def purge(self, max_age=14, msg_type=False):
        for i in self.catalogue:#Parcours du catalogue
            if i.msg_type==msg_type:#Si message selectionne du même type que ceux que l'on souhaite supprimer
                if i.age(True)>=max_age: #Si le message est trop vieux
                    self.catalogue.remove(i)#Suppression du message
       
    #Check if message string is in a category
    def check_msg(self, msg_str, msg_type): 
        etat=False 
        for i in self.catalogue: #Parcours du catalogue
             if i.msg_type==msg_type and i.msg==msg_str: #Si on a trouve le message
                 etat=True #Variable booleenne disant qu'on a trouve
        return etat 

	#Say if I should quarantine based on the state of my catalog
    def quarantine(self, nb_heard=3):
        self.purge() #Suppression des messages trop vieux
        nb=0
        for i in self.catalogue(): #Parcours du catalogue
            if i.msg_type==Message.MSG_IHEARD: #On s'interesse aux messages qu'on a entendu
                if self.check_msg(i.msg,Message.MSG_THEY): #On regarde si les messages qu'on a entendu sont ceux envoyes par l'hopital
                    nb=nb+1
        if nb_heard<nb : #Pas cas contact
            return False
        else : #Cas contact
            return True            
    
#will only execute if this file is run
if __name__ == "__main__": 
    #test the class
    
    #Ajout de messages
    catalog = MessageCatalog("test8.json") #Creation ou importation catalogue
    catalog.add_message(Message())#Ajout d'un message au catalogue
    myMessage=Message()#Creation, d'un deuxieme message
    catalog.add_message(myMessage)
    catalog.add_message(Message(myMessage.generate(), myMessage.MSG_THEY))#Ajout de ce message au catalogue
    time.sleep(0.5)#Attente
    catalog.add_message(Message(myMessage.generate(),myMessage.MSG_IHEARD)) #Troisieme message
    catalog.add_message(Message())#Quatrième message
    time.sleep(0.5)#Attente
    catalog.add_message(Message())#Cinquieme message
    
    #Exportation
    catalog.c_export()#Exportation catalogue
    #catalog.c_export_type(Message.MSG_THEY)#Exportation catalogue d'un type donnee de messages
    
    #Test des fonctions
    print(catalog.check_msg(myMessage.msg,myMessage.msg_type))
    





