## -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 14:52:22 2020

@author: Alexia Rousset
"""
#TERMINE!!

#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
import json
#My libraries
from MessageCatalog import MessageCatalog
from Message import Message
from Client import Client

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])
def index():
    return 'Tout marche!'

	#case 2 hear message de la part du client
@app.route('/<msg>', methods=['POST']) #Si on a recu un message I SAID entendu par un client
def add_heard(msg):
    #print("add catalog "+msg) #Affichage des infos sur le message
    hopital.catalog.add_message(Message(msg,Message.MSG_IHEARD))#Ajout du message entendu au catalogue hopital. Type du message devient I HEARD
    return Response("Reçu",status=201) #Retourne une reponse disant qu'on a bien recu le message

#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST'])
def hospital():
    if request.method=='GET': #Cas ou le client veut recevoir la liste they-said de l'hopital
        hopital.catalog.purge(Message.MSG_THEY) #Suppression des messages trop vieux
        response=Response(hopital.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #Renvoyer les messages They Said dans un fichier de format JSON si la requête s'est bien déroulée
    else: # Cas un client veut envoyer sa liste des I said à l'hopital
        if request.is_json: #Si on a recu le fichier json du client
            res=request.get_json()#on récupère le fichier json du client
            res=json.loads(res)
            hopital.catalog.c_import_type(Message.MSG_THEY,res) #On importe dans le catalogue hopital les messages ISAID des clients que l'on a transforme en message THEY_SAID 

            response = Response("Recu", mimetype='text/plain', status=201)#Retourne une reponse disant qu'on a bien recu les messages
    return response
            
#End hospital use case
#will only execute if this file is run
if __name__ == "__main__":
    hopital=Client("hopital.json")
    app.run(host="0.0.0.0", debug=False)



